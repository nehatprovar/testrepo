package pageobjects;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.provar.core.testapi.annotations.*;

@Page( title="test"                                
     , summary=""
     , relativeUrl=""
     , connection="Admin"
     )             
public class test {

	@TextType()
	@FindBy(xpath = "//input[@id='twotabsearchtextbox']")
	public WebElement search;
			
}
